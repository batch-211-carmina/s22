// console.log("Hello World!")


//ARRAY METHODS
	//JS has built-in functions and methods for arrays. This allows us to manipulate and access array items.


	//Mutator Methods
	/*
		- Mutator Methods are functions that "mutate" or change an array after they're created
		- These methods manipulate the original array performing various tasks such as adding and removing elements
	*/

		let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon Fruit'];

		//push() method
		/*
			- adds an element in the end of an array AND returns the array's length

			SYNTAX:
				arrayName.push();
		*/

		console.log('Current Array: ');
		console.log(fruits); //log fruits array
		let fruitsLength = fruits.push('Mango'); //5
		console.log(fruitsLength);
		console.log("Mutated array from push method");
		console.log(fruits); //Mango is added at the end of the array


		fruits.push('Avocado', 'Guava');
		console.log("Mutated array from push method: ");
		console.log(fruits);


		//pop() method
		/*
			- Removes the last element in an array AND returns the removed element

			SYNTAX:
				arrayName.pop();
		*/

		let removedFruit = fruits.pop();
		console.log(removedFruit);  //Guava
		console.log("Mutated array from pop method:");
		console.log(fruits);  //Guava is removed at the end of the array


		/*MINI ACTIVITY
			Create a function which will unfriend the last person in the array
			Log the ghostFighters array in our console
		*/

		let ghostFighters = ['Eugene', 'Dennis', 'Alfred', 'Taguro'];

		function unfriend(){
			let removedFriend = ghostFighters.pop();
			console.log(removedFriend);
		}

		unfriend();
		console.log(ghostFighters);


		//unshift() method
		/*
			- Adds one or more elements at the beginning of an array

			SYNTAX:
				arrayName.unshift('elementA');
				arrayName.unshift('elementA', elementB);
		*/

		fruits.unshift('Lime', 'Banana');
		console.log("Mutated array from unshift method:");
		console.log(fruits);


		//shift() method
		/*
			- Removes an element at the beginning of an array AND returns the removed element

			SYNTAX:
				arrayName.shift();
		*/

		let anotherFruit = fruits.shift();
		console.log(anotherFruit); //Lime
		console.log("Mutated array from shift method:")
		console.log(fruits);  //the Lime element is removed from an array


		//splice() method
		/*
			- Simultaneously removes elements from a specified index number and adds elements

			SYNTAX:
				arrayName.splice(startingIndex, deleteCount, elementsToBeAdded);
		*/

		fruits.splice(1, 2, 'Lime', 'Cherry');  //start at index1, will delete 2 elements then will add 'Lime' and 'Cherry'
		console.log("Mutated array from splice method:");
		console.log(fruits);


		//sort() method
		/*
			- Rearranges the array elements in the alphanumeric order
			
			SYNTAX:
				arrayName.sort();
		*/
		fruits.sort();
		console.log("Mutated array from sort method:");
		console.log(fruits);


		//revers() method
		/*
			- Reverses the order array elements
			
			SYNTAX:
				arrayName.reverse();
		*/
		fruits.reverse();
		console.log("Mutated array from reverse method:");
		console.log(fruits);



	//Non-Mutator Methods
	/*
		- Non-Mutator methods are functions that do not modify or change an array after they are created
		- These methods do not manipulate the original array performing various tasks such as returning elements from an array and also combining arrays and printing the output
	*/

		let countries = ['US', 'PH', 'CAN', 'SG', 'TH', 'PH', 'FR', 'DE'];


		//indexOf() method
		/*
			- Returns the index number of the first matching element found in an array
			- If no match is found, the result is -1
			- The search process will be done from first element proceeding to the last element
			
			SYNTAX:
				arrayName.indexOf(searchValue);
				arrayName.indexOf(searchValue, fromIndex);
		*/

		let firstIndex = countries.indexOf('PH'); //1
		// let firstIndex = countries.indexOf('PH',3); //5
		console.log("Result of indexOf method: " + firstIndex);

		let invalidCountry = countries.indexOf('BR');
		console.log("Result of indexOf method: " + invalidCountry);


		//lastIndexOf() method
		/*
			- Returns the index number of the last matching element found in an array
			- The search process will be done from the last element proceeding to the first element

			SYNTAX:
				arrayName.lastIndexOf(searchValue);
				arrayName.lastIndexOf(searchValue, fromIndex);
		*/

		let lastIndex = countries.lastIndexOf('PH');  //5
		// let lastIndex = countries.lastIndexOf('PH', 3);  //1
		console.log("Result of the lastIndexOf method: " + lastIndex);


		//slice() method
		/*
			- Portions/slices elements from an array AND returns a new array
		
			SYNTAX:
				arrayName.slice(startingIndex);
				arrayName.slice(startingIndex, endingIndex);
		*/
		console.log("Original Countries Array: ");
		console.log(countries);
		let slicedArrayA = countries.slice(2);
		console.log("Result from slice method:");
		console.log(slicedArrayA);


		let slicedArrayB = countries.slice(2,4);
		console.log("Result from slice method:");
		console.log(slicedArrayB);


		let slicedArrayC = countries.slice(-3);  //slice the 3 element at the end
		console.log("Result from slice method:");
		console.log(slicedArrayC);


		//toString() method
		/*
			- Returns an array as a string separated by commas

			SYNTAX:
				arrayName.toString();
		*/
		let stringArray = countries.toString();
		console.log("Result from toString method: ");
		console.log(stringArray);


		//concat() method
		/*
			- Combines two arrays and returns the combined result

			SYNTAX:
				arrayA.concat(arrayB);
				arrayA.concat(elementA);
		*/
		let taskArrayA = ['drink html', 'eat javascript'];
		let taskArrayB = ['inhale css', 'breathe sass'];
		let taskArrayC = ['get git', 'be node'];

		let tasks = taskArrayA.concat(taskArrayB);
		console.log("Result from concat method: ");
		console.log(tasks);


		//Combining multiple arrays
		console.log("Result from concat method: ");
		let allTasks = taskArrayA.concat(taskArrayB,taskArrayC);
		console.log(allTasks);


		//Combining arrays with elements
		let combinedTasks = taskArrayA.concat('smell express', 'throw react');
		console.log("Result from concat method: ");
		console.log(combinedTasks);


		//join() method
		/*
			- Returns an array as a string separated by specified separator

			SYNTAX:
				arrayName.join('separatorString');
		*/
		let users = ['John', 'Jane', 'Doe', 'Robert', 'Nej'];
		console.log(users.join()); //separated by comma
		console.log(users.join(' '));  //separated by space
		console.log(users.join('-'));  //separated by dash


	
	//Iteration method
	/*
		- Iteration methods are loops designed to perform repetitive tasks on arrays
		- Iteration methods loops over all items in an array
		- Useful for manipulating array data resulting in complex tasks
	*/

		//forEach() method
		/*
			- Similar to a for loop that iterates on each array element
			- for each item in the array, the anonymous function passed in the forEach() method will be run
			- The anonymous function is able to receive the current item being iterated or loop over by assigning a parameter
			- Variable names for arrays are normally written in the plural form of the data stored in an array
			- It's a common practice to use the singular form of the array content for parameter names used in array loops
			- forEach() does NOT return anything

			SYNTAX:
				arrayName.forEach(function(individualElement)){
					statement;
				};
		*/
		//task is just a naming convention - you can name it anything
		allTasks.forEach(function(task){
			console.log(task);
		});

		/*
			MINI ACTIVITY
			create a function that can display the ghostFighters one by one in our console
			Invoke the function
		*/

		function displayGhostFighters(){
		ghostFighters.forEach(function(g){
			console.log(g);
		});
		}
		displayGhostFighters();


		//Using forEach with conditional statements

			//Looping through all arrays items
			/*
				- It is good practice to print the current element in the console when working with array iteration methods to have an idea of what information is being worked on for each iteration of the loop

				- Creating a separate variable to store results of an array iteration methods are also good practice to avoid confusion by modifying the original array

				- Mastering loops and arrays allow us developers to perform a wide range of features that help in data management and analysis
			*/

		let filteredTasks = [];

		allTasks.forEach(function(task){

			console.log(tasks);

			if(task.length>10){
				console.log(task);  //will print the tasks that are more than 10 characters
				filteredTasks.push(task);
			}
		});

		console.log("Result of filtered tasks:");
		console.log(filteredTasks); //returns the filtered tasks that are more that 10 char


		//map() method
		/*
			- Iterates on each element AND returns new array with different values depending on the result of the function's operation

			SYNTAX:
				let/const resultArray = arrayName.map(function(individualElement){
					return expression/condition;
				});
		*/

		let numbers = [1, 2, 3, 4, 5];

		let numberMap = numbers.map(function(number){
			return number * number;
		});

		console.log("Original Array: ");
		console.log(numbers);
		console.log("Result of map method: ");
		console.log(numberMap);


		//map() vs forEach()

		let numberForEach = numbers.forEach(function(number){
			return number * number;
		});

		//forEach does not return new array
		console.log(numberForEach); //undefined


		//every() method
		/*
			- Checks if all elements in an array meet the given conditions
			- This is useful for validating data stored in an arrays especially when dealing with large amounts of data
			- Returns true if all elements meet the condition, otherwise false

			SYNTAX:
				let/const resultArray = arrayName.every(function(individualElement){
					return expression/condition;
				})
		*/

		let allValid = numbers.every(function(number){
			return (number < 3);
		});

		console.log("Result of every method: ");
		console.log(allValid);  //false


		//some() method
		/*
			- Checks if at least one element in the array meets the give condition
			- Returns a true value if at least one element meets the condition and false if otherwise

			SYNTAX:
				let/const resultArray = arrayName.some(function(individualElement){
					return expression/condition;
				});
		*/

		let someValid = numbers.some(function(number){
			return (number<2);
		});

		console.log("Result of some method: ");
		console.log(someValid);  //true

		//Combining the returned result from every/some method may be used in other statements to perform consecutive results
		if(someValid){
			console.log('Some numbers in the array are greater that 2');
		}


		//filter
		/*
			- Returns new array that contains elements which meets a given condition
			- Returns an empty array if no elements were found

			SYNTAX:
				let/const resultArray = arrayName.filter(function(individualElement){
					return expression/condition;
				});
		*/

		let filteredValid = numbers.filter(function(number){
			return (number<3);
		});

		console.log("Result of filter method: ");
		console.log(filteredValid);  // [1,2]
	

		let nothingFound = numbers.filter(function(number){
			return (number = 0);
		});

		console.log("Result of filter method: ");
		console.log(nothingFound);  // [] empty array


		//Filtering using forEach

		let filteredNumbers = [];

		numbers.forEach(function(number){
			console.log(number);

			if(number<3){
				filteredNumbers.push(number);
			}
		});

		console.log("Result of filter method using forEach: ");
		console.log(filteredNumbers);


		//includes()
		/*
			- Checks if the arguments passed can be found in the array
				- it returns a boolean which can be saved in a variable
				- returns true if the argument is found the array
				- returns false if it is not

			SYNTAX:
			arrayName.include(<argumentToFind>)
		*/

		let products = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

		let productFound = products.includes("Mouse");
		console.log(productFound);

		let productNotFound = products.includes("Headset");
		console.log(productNotFound);


		//Method Chaining
		/*
			- Methods can be chained using them one after another
			- The result of the first method is used on the second method until all 'chained' methods have been resolved
		*/

		let filteredProducts = products.filter(function(product){
			return product.toLowerCase().includes('a');
		});

		console.log(filteredProducts);


		/*
			Mini Activity
			Create an addTrainer function that will enable us to add a trainer
			in the contacts array
			 -- This function should be able to receive a string
			 -- Determine if the added trainer already exists in the contacts array:
			 -- if it is, show an alert saying "Already added in the Match Call"
			 -- if it is not, add the trainer in the contacts array and show an alert saying 'Registered!'
			 -- invoke and add a trainer in the browser's console
			 -- in the console, log the contacts array
		*/

		let contacts = ['Ash'];

		function addTrainer(trainer){

			if(contacts){
				alert("Already added in the Match Call");
			}
			else{
				contacts.push(trainer);
				alert('Registered!');
			}
		};


		//solution
		/*let contacts = ['Ash'];

		function addTrainer(trainer){
			
			let doesTrainerExist = contacts.includes(trainer);

			if(doesTrainerExist){
				alert("Already added in the Match Call");
			}
			else{
				contacts.push(trainer);
				alert('Registered!');
			}
		};*/


		//reduce() method
		/*
			- Evaluates elements from left to right and returns/reduces the array into a single value

			SYNTAX:
				let/const resultArray = arrayName.reduce(function(accumulator, currentValue){
					return expression/operation;
				});

			- "Accumulator" parameter in the function stores the result for every iteration of the loop
			- "currentValue" is the curretn/next element in the array that is evaluated in each iteration of the loop


			How the 'reduce' method works
			1. The first/result element in the array is stored in the 'accumulator' parameter
			2. The second/next element in the array is stored in the currentValue parameter
			3. An operation is performed on the two elements
			4. The loop repeats step 1-3 until all elements have been worked on
		*/

		console.log(numbers);
		let iteration = 0;
		let iterationStr = 0;

		let reducedArray = numbers.reduce(function(x,y){
			console.warn('current iteration: ' + ++iteration);
			console.log('accumulator: ' + x);
			console.log('current value: ' + y);

			return x + y;
		});

		console.log("Result of reduce method: " + reducedArray);

		let list = ["Hello", "Again", "World"];

		let reducedJoin = list.reduce(function(x,y){
			console.warn('current iteration: ' + ++iteration);
			console.log('accumulator: ' + x);
			console.log('current value: ' + y);

			return x + ' ' + y;
		});

		console.log("Result of reduce method: " + reducedJoin);